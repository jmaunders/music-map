import pThrottle from 'p-throttle'
import Config from 'Config'
import Geocode from 'react-geocode'
Geocode.setApiKey(Config.GoogleMapsKey)

const location_services = {
  getArtistLocation: pThrottle(artist_name => {
    return new Promise((resolve, reject) => {
      fetch(`http://musicbrainz.org/ws/2/artist?limit=1&fmt=json&query=${artist_name}`)
        .then(response => response.json())
        .then(data => {
          let locationString
          if (data.artists[0]['begin-area']) {
            locationString = data.artists[0]['begin-area'].name
            if (data.artists[0]['area'])
              locationString += (', ' + data.artists[0]['area'].name)
          } else if (data.artists[0]['area']) {
            locationString = data.artists[0]['area'].name
          }
  
          return locationString
        })
        .then(artist_location => {
          if (artist_location)
            resolve(location_services.locationToCoords(artist_location))
          else
            resolve()
        })
        .catch(err => {
          console.log({err})
          resolve()
        }) 
    })
  }, 1, 1000),
  
  locationToCoords: (location_name) => {
    return new Promise((resolve, reject) => {
      Geocode.fromAddress(location_name)
        .then(result => {
          let { lat, lng } = result.results[0].geometry.location
          resolve({lat, lng})
        })
        .catch(err => {
          console.log(err)
          reject(err)
        })
    })
  } 
}

module.exports = location_services