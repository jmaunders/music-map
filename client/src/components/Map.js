import React, { Component } from 'react'
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps'
import MapMarker from './MapMarker'
const mapStyles = require('../styles/mapStyle.json')

const Map = withScriptjs(withGoogleMap(props => {
  const bounds = new google.maps.LatLngBounds()


  return (
  <GoogleMap
    ref={map => map && map.fitBounds(bounds)}
    defaultZoom={10}
    defaultCenter={{ lat: 0, lng: 0 }}
    defaultOptions={{ styles: mapStyles}}
  >
    {props.artists.map(item => {
      if (item.location) {
        bounds.extend(item.location)

        return <MapMarker
          key={item.index}
          artist={item}
        />
      }
    })}
  </GoogleMap>
  )
}))

export default Map