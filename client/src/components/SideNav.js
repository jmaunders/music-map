import React, { Component } from 'react'
import { AppContext } from './App'
import MapSelector from './MapSelector'

import Sidebar from 'grommet/components/Sidebar'
import Header from 'grommet/components/Header'
import Title from 'grommet/components/Title.js'
import Box from 'grommet/components/Box.js'
import Button from 'grommet/components/Button'
import CloseIcon from 'grommet/components/icons/base/Close'
import Footer from 'grommet/components/Footer.js'

class SideNav extends Component {
  render() {
    return (
      <Sidebar className='side-nav' colorIndex='neutral-1' size='small'>
        <Header className='side-nav-header' pad='medium' justify='between'>
          <Title>
            MapTuune
          </Title>
          <Button 
            className="mobile-close"
            onClick={this.props.togglenav} 
            icon={<CloseIcon />} 
          />
        </Header>
        <Box full='horizontal' flex='grow' justify='start'>
          <AppContext.Consumer>
            {value =>
              !value.accessToken ?
                <Button href={'http://127.0.0.1:3001/login'} className='active' primary={true} label='Login' fill={false} /> :
                <MapSelector accessToken={value.accessToken} />
            }
          </AppContext.Consumer>
        </Box>
        <Footer pad='medium'>
          &copy; MapTuune
          </Footer>
      </Sidebar>
    )
  }
}

export default SideNav
