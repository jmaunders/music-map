import React, { Component } from 'react'

import Box from 'grommet/components/Box.js'
import Heading from 'grommet/components/Heading'
import Button from 'grommet/components/Button'
import MenuIcon from 'grommet/components/icons/base/Menu'
import Paragraph from 'grommet/components/Paragraph'

class HomeView extends Component {
  render () {
    return (
      <Box
        colorIndex='neutral-2'
        full
        align='center'
        textAlign='center'
      >
        <Button 
          className="mobile-hamburger"
          onClick={this.props.togglenav} 
          icon={<MenuIcon />} 
        />
        <div className='home-heading'>
          <Heading>MapTuune</Heading>
          <Paragraph>Home details will go here...</Paragraph>
        </div>
      </Box>
    )
  }
}

export default HomeView
