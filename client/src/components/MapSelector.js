import React, { Component } from 'react'

import Menu from 'grommet/components/Menu.js'
import Anchor from 'grommet/components/Anchor'
import { AppContext } from './App';

class MapSelector extends Component {
  constructor (props) {
    super(props)
  }

  render() {
    return (
      <Menu primary={true} fill={true}>
        <AppContext.Consumer>
          {value => <Anchor onClick={value.setCurrentView.bind(this, 'Top Artists')} className={value.currentView === 'Top Artists' ? 'active' : ''}>Top Artists</Anchor>}
        </AppContext.Consumer>
        <AppContext.Consumer>
          {/* {value => <Anchor onClick={value.setCurrentView.bind(this, 'Recent Artists')} className={value.currentView === 'Recent Artists' ? 'active' : ''}>Recent Artists</Anchor>} */}
          {value => <Anchor className="coming-soon-anchor">Recent Artists<span>Coming Soon</span></Anchor>}
        </AppContext.Consumer>
        {/* <Anchor>Select Playlist</Anchor> */}
      </Menu>
    )
  }
}

export default MapSelector
