import React, { Component } from 'react'
import { AppContext } from './App'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Paragraph from 'grommet/components/Paragraph'
import Button from 'grommet/components/Button'
import MenuIcon from 'grommet/components/icons/base/Menu'
import Spinning from 'grommet/components/icons/Spinning'
import Map from './Map'

import location_services from '../service/location'
import Config from 'Config'

class MainView extends Component {
  constructor () {
    super()
    this.state = {
      artistList: ''
    }
  }

  fetchArtistData(token, view) {
    this.setState({ artistList: '' })
    switch (view) {
      case 'Top Artists':
        fetch('https://api.spotify.com/v1/me/top/artists?limit=20', {
          headers: { Authorization: 'Bearer ' + token }
        })
          .then(response => response.json())
          .then(data => {
            return data.items.map(async (item, index) => {
              let obj = {
                index: index,
                id: item.id,
                name: item.name,
                image: item.images[0].url,
                followers: item.followers.total,
                location: await location_services.getArtistLocation(item.name)
              }
              return obj
            })
          })
          .then(arr => Promise.all(arr))
          .then(result => {
            this.setState({ artistList: result })
          })
          .catch(err => {
            console.log(err)
            this.setState({ artistList: '' })
          })
        break
      case 'Recent Artists':
        fetch('https://api.spotify.com/v1/me/player/recently-played?limit=20', {
          headers: { Authorization: 'Bearer ' + token }
        })
          .then(response => response.json())
          .then(data => {
            console.log(data)
            return data.items.map(async (item, index) => {
              let obj = {
                index: index,
                item: item.track.artists[0].id,
                name: item.track.artists[0].name,
                location: await location_services.getArtistLocation(item.track.artists[0].name)
              }

              return obj
            })
          })
          .then(arr => Promise.all(arr))
          .then(result => {
            this.setState({ artistList: result })
          })
          .catch(err => {
            console.log(err)
            this.setState({ artistList: '' })
          })
        break
      default:
        this.setState({ artistList: '' })
    }
  }

  componentDidMount () {
    if (this.props.context.accessToken) {
      this.fetchArtistData(this.props.context.accessToken, this.props.context.currentView)
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.context.currentView !== this.props.context.currentView)
      this.fetchArtistData(nextProps.context.accessToken, nextProps.context.currentView)
  }
  
  render () {
    console.log(this.props)
    return (
      <div>
        <Button 
          className="mobile-hamburger main"
          onClick={this.props.togglenav} 
          icon={<MenuIcon />} 
        />
        <Box
          colorIndex='neutral-2'
          justify='center'
          align='center'
          pad='medium'
          full
        >
          {
            this.state.artistList ?
            <Box full={true}>
              <Heading tag='h2'>{this.props.context.currentView}</Heading>
              <Box className="map-wrapper" full={true}>
                <Map
                  googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${Config.GoogleMapsKey}&v=3.exp&libraries=geometry,drawing,places&style=element:geometry.stroke%7Ccolor:0x865CD6`}
                  loadingElement={<div style={{ height: `100%` }} />}
                  containerElement={<div style={{ height: `100%`, width: '100%' }} />}
                  mapElement={<div style={{ height: `100%` }} />}
                  artists={this.state.artistList}
                />
              </Box>
            </Box> :
            <Box
              align='center'
              justify='center'
              textAlign='center'
            >
            <Spinning
              size='large'
            />
            <Paragraph size='large'>Please wait whilst we put together your map...</Paragraph>
            <Paragraph size='small'>(Been waiting longer than a minute? We recommend that you refresh!)</Paragraph>
            </Box>
          }
        </Box>
      </div>
    )
  }
}

export default props => (
  <AppContext.Consumer>
    {value => <MainView togglenav={props.togglenav} context={value} />}
  </AppContext.Consumer>
)
