import React, { Component } from 'react'
import Layout from './Layout'

window.getUrlParameter = name => {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

export const AppContext = React.createContext()

class AppProvider extends Component {
  constructor () {
    super()
    this.state = {
      accessToken: getUrlParameter('access_token'),
      setAccessToken: token => {
        this.setState({ accessToken: token })
      },
      currentView: !getUrlParameter('access_token') ? 'Home' : 'Top Artists',
      setCurrentView: view => {
        this.setState({ currentView: view })
      }
    }
  }

  render () {
    return (
      <AppContext.Provider value={this.state}>
        {this.props.children}
      </AppContext.Provider>
    )
  }
}

class App extends Component {
  render () {
    return (
      <AppProvider>
        <Layout />
      </AppProvider>
    )
  }
}

export default App
