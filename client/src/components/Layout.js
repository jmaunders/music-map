import React, { Component } from 'react'
import App from 'grommet/components/App.js'
import Split from 'grommet/components/Split.js'

import SideNav from './SideNav'
import MainView from './MainView'
import HomeView from './HomeView'
import { AppContext } from './App';

class Layout extends Component {
  constructor() {
    super()

    this.state = {
      mobileNavOpen: false
    }

    this.toggleMobileNav = this.toggleMobileNav.bind(this)
  }

  toggleMobileNav() {
    this.setState({ mobileNavOpen: !this.state.mobileNavOpen })
  }

  render() {
    return (
      <App className='app'>
        <Split priority={!this.state.mobileNavOpen ? 'right' : 'left'} flex='right' fixed>
          <SideNav togglenav={() => this.toggleMobileNav()} />
          <AppContext.Consumer>
            {value => 
              value.accessToken ?
              <MainView togglenav={() => this.toggleMobileNav()} /> :
              <HomeView togglenav={() => this.toggleMobileNav()} />
            }
          </AppContext.Consumer>
        </Split>
      </App>
    )
  }
}

export default Layout
