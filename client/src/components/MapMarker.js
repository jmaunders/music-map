import React, { Component } from 'react'
import { Marker, InfoWindow } from "react-google-maps"

class MapMarker extends Component {
  constructor() {
    super()
    this.state = {
      infoOpen: false
    }
    this.toggleInfo = this.toggleInfo.bind(this)
  }

  toggleInfo() {
    let toChangeTo = !this.state.infoOpen
    this.setState({
      infoOpen: toChangeTo
    })
  }

  render() {
    return (
      <Marker
        id={this.props.artist.id}
        // label={this.props.artist.name}
        position={{ lat: this.props.artist.location.lat, lng: this.props.artist.location.lng }}
        onClick={this.toggleInfo}
      >
      {
        this.state.infoOpen && 
        <InfoWindow onCloseClick={this.toggleInfo}>
          <div className="artist-info">
            <img className="artist-image" src={this.props.artist.image} />
            <div>{this.props.artist.name}</div>
            <div>Followers: {this.props.artist.followers}</div>
          </div>
        </InfoWindow>
      }
      </Marker>
    )
  }
}

export default MapMarker