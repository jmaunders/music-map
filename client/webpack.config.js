const HtmlWebPackPlugin = require("html-webpack-plugin")

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
})

module.exports = (env, argv) => ({
  entry: ['babel-polyfill', './src/index.js'],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader"
          },
          {
            loader: "sass-loader", options: {
              includePaths: ['./node_modules', './src/styles/main.scss']
            }
          }
        ]
      }
    ]
  },
  plugins: [htmlPlugin],
  externals: {
    'Config': JSON.stringify(argv.mode === 'production' ? require('./config.prod.json') : require('./config.dev.json'))
  }
})