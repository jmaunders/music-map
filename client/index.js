const Koa = require('koa')
const app = new Koa()
const serve = require('koa-static')

app.use(serve('./dist'))

app.listen(3000)
console.log('Maptuune Client Listening on 3000')