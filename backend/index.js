const Koa = require('koa')
const app = new Koa()
const route = require('koa-path-match')({})
const bodyParser = require('koa-bodyparser')
const querystring = require('querystring')
const axios = require('axios')

require('dotenv').config()
const redirect_uri = process.env.REDIRECT_URI

// Middleware
app.use(bodyParser())
require('koa-qs')(app, 'first')
app.use(require('@koa/cors')())
app.use(async (ctx, next) => {
  if (!ctx.response.headers['Access-Control-Allow-Origin']) {
    ctx.set('Access-Control-Allow-Origin', '*')
  }
  await next()
})

// Server start / Routess
app.listen(3001, () => console.log('Music Map Backend - Started on 3001'))

app.use(
  route('/login').get(ctx => {
    ctx.redirect(
      'https://accounts.spotify.com/authorize?' +
        querystring.stringify({
          response_type: 'code',
          client_id: process.env.SPOTIFY_CLIENT_ID,
          scope: 'user-read-email user-top-read user-read-recently-played',
          redirect_uri
        })
    )
  })
)

app.use(
  route('/callback').get(async ctx => {
    await axios
      .post('https://accounts.spotify.com/api/token', querystring.stringify({
        code: ctx.query['code'], // Temp code from Spotify API
        redirect_uri: redirect_uri,
        grant_type: 'authorization_code',
        client_id: process.env.SPOTIFY_CLIENT_ID,
        client_secret: process.env.SPOTIFY_CLIENT_SECRET
      }))
      .then(response => {
        ctx.redirect(
          process.env.FRONTEND_URI + '?access_token=' + response.data.access_token
        )
      })
      .catch(err => {
        console.log(err.data)
        ctx.redirect(process.env.FRONTEND_URI_ERROR)
      })
  })
)

app.use(async (ctx, next) => {
  if (ctx.url === '/hc') ctx.body = `I'm alive!`
  await next()
})
